import axios from 'axios'
import { Unibabel } from 'unibabel/index'
import yaml from 'js-yaml'

const apiBaseUrl = 'https://gitlab.com/api/v4/projects/bastianmelnyk%2Fshop'

// fix UTF-8 in base64
const atob = process.browser
  ? Unibabel.base64ToUtf8
  : input => Buffer.from(input, 'base64').toString('utf8')

async function fetchDirectories(path) {
  const { data: categories } = await axios.get(
    `${apiBaseUrl}/repository/tree?path=${encodeURIComponent(path)}`
  )

  return categories
    .filter(entry => entry.type === 'tree')
    .map(entry => entry.name)
}

async function fetchYamlContent(filePath) {
  const { data } = await axios.get(
    `${apiBaseUrl}/repository/files/${encodeURIComponent(filePath)}?ref=master`
  )
  const { content } = data
  const contentDecoded = atob(content)

  return yaml.safeLoad(contentDecoded)
}

export async function fetchCategories() {
  return await fetchDirectories('products')
}

export async function fetchCategoryData(categoryId) {
  return await fetchYamlContent(`products/${categoryId}/category.yml`)
}

export async function fetchCategoryProducts(categoryId) {
  return await fetchDirectories(`products/${categoryId}`)
}

export async function fetchProductData(categoryId, productId) {
  return await fetchYamlContent(
    `products/${categoryId}/${productId}/product.yml`
  )
}
