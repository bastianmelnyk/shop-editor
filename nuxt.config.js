const pkg = require('./package')

import { fetchCategories, fetchCategoryProducts } from './lib/api'

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {}
  },

  /*
  ** Customize the generated output folder
  */
  generate: {
    dir: 'public',

    async routes() {
      const categoryIds = await fetchCategories()
      const productRoutes = (await Promise.all(
        categoryIds.map(async function(categoryId) {
          const productIds = await fetchCategoryProducts(categoryId)
          return productIds.map(id => `/${categoryId}/${id}`)
        })
      )).reduce((a, b) => a.concat(b), [])

      return [...categoryIds.map(id => `/${id}`), ...productRoutes]
    }
  },

  /*
  ** Customize the base url
  */
  router: {
    base: '/shop-editor/'
  }
}
